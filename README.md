Информация о волуме

```
docker volume ls
DRIVER    VOLUME NAME
local     less18_static
======================
docker volume inspect less18_static
[
    {
        "CreatedAt": "2021-07-11T13:26:13Z",
        "Driver": "local",
        "Labels": {
            "com.docker.compose.project": "less18",
            "com.docker.compose.version": "1.29.2",
            "com.docker.compose.volume": "static"
        },
        "Mountpoint": "/var/lib/docker/volumes/less18_static/_data",
        "Name": "less18_static",
        "Options": {
            "device": "/home/shipovalovvs/less1.8/nginx",
            "o": "bind",
            "type": "none"
        },
        "Scope": "local"
    }
]
```

